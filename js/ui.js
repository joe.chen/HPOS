// JavaScript Document
 
var ui = (function () {
    var press_id = '';
    var is_pageout = false;
    var show = function (id) {
        document.getElementById(id).style.display = "block";
    };
    var hide = function (id) {
        document.getElementById(id).style.display = "none";
    };
    var gopage = function (src, delay) {
        if (!is_pageout) {
            is_pageout = true;
            try {
                if (parent.str.isNull(delay)) {
                    delay = 0;
                }
                console.info("from page to:" + parent._ifContent.src + "==>" + src);
                if (delay > 0) {
                    setTimeout(function () {
                        parent._ifContent.src = src + ".htm?" + Math.random();
                    }, delay);
                } else {
                    parent._ifContent.src = src + ".htm?" + Math.random();
                }
            } catch (err) {
                // alert(err.Message);
            }

        }
    };
    var gomenu = function () {
        if (parent._ifContent.src.indexOf('/menu.htm') == -1)
            gopage('menu');
    };
    var get = function (id) {
        return document.getElementById(id);
    };
    var getMsg = function (code) {
    }
    return {
        press_id: press_id,
        get: get,
        show: show,
        hide: hide,
        gomenu: gomenu,
        is_pageout: is_pageout,
        gopage: gopage,
        setTimeout: setTimeout

    };
})();