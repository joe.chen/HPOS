// JavaScript Document
var BindAsEventListener = function(object, fun) {  

    var args = Array.prototype.slice.call(arguments,2);  
   
    return function(event) {  
        return fun.apply(object, [event || window.event].concat(args));  
    }  
}  
var Table = Class.create();  
Table.prototype = {  
    //初始化  
    Init:function(obj,options){  
	    this.Document = document;
		this.Window = window;
        this.Table=$(obj);  
        if(!this.Table){return;}  
         
		this.Table.className = "ttable";  
        this.SetOptions(options);//设置参数  
          
        this.HoverColor=this.Options.hoverColor;  
        this.ActiveColor=this.Options.activeColor;  
        this.Hover=this.Options.hover;  
        this.Choose=this.Options.choose;  
        this.Multiple=this.Options.multiple;  
        this.Cancel=this.Options.cancel;  
        this.Title=this.Options.title;  
        this.DClick=this.Options.dclick;  
        this.OnClick=this.Options.onClick;  
		this.onKeymove = this.Options.onKeymove;
        this.OnCancel=this.Options.onCancel;  
        this.OnMove=this.Options.onMove;  
        this.OnDClick=this.Options.onDClick; 
		this.tableth = this.Options.tableth; 
        this.pageSize = this.Options.pageSize;
		this.setpage = this.Options.setpage;
		this.tdwidth = this.Options.tdwidth;
		this.colname = this.Options.colname;
		this.colformat = this.Options.colformat;
		
        this._AddMouseEvent();  //添加事件  
		this._createShowHeader();
          
        this._CurrentRow=null; //当前移动行  
		this._CurrentIndex = -1;
		this._pageIndex = -1;//当前页
        this._Cursor = null;//数据集游标
	
		this._maxpage = -1;
		this._dataobject = new Array(); 
		
        this.SelectedArray=new Array();//设置或返回被选中行的集合  
    },  
	
	//设置参数  
    SetOptions:function(options){  
        this.Options={  
            hoverColor:"#ffa800",        //移动时背景色  
            activeColor:"#ffa800",       //选中时背景色  
            hover:false,                  //是否开启移动行背景色  
            choose:true,                //是否开启选择  
            multiple:false,             //是否开启多选,开启后按Ctrl可以多选,取消功能需要同时开启  
            title:true,                 //是否提示title  
            cancel:true,                //是否开启取消功能  
            dclick:true,                //是否双击
			pageSize:8,
			tableth:[],
			tdwidth:[],
			colname:[],
			colformat:[],
            onClick:function(){},       //选中时附加方法  
            onCancel:function(){},       //取消选择时附加方法，此时取消功能必须开启  
            onMove:function(){},         //鼠标移动事件 
			onKeymove:function(){}, 
		    setpage:null,
            onDClick:function(){}       //双击  
		
        };  
        Extend(this.Options,options||{});  
    },  
	
	 _createShowHeader:function()
{    
	//创建表头行到文档树中 
	   var node=this._GetParentNode();  
	    for(var i =node.rows.length-1;i>=0;i--)
		 {
			  node.deleteRow(i); 
		 }
        var row=document.createElement("tr");  
		if(this.tableth==null)
		return;
		
        for(var i=0;i<this.tableth.length+1;i++){  
            var cell=document.createElement("th");  
			cell.align="left";
			if(i==0)
			{
            cell.innerHTML=this.tableth[i]; 
			cell.width = this.tdwidth[i];
			cell.style.display= "none"; 
			
			}
			else if(i==1)
			{
				cell.innerHTML="序号";
				cell.width = "70px";
			}
			else
			{
				if(this.tdwidth[i-1]==0)
				    cell.style.display= "none"; 
				  cell.innerHTML=this.tableth[i-1]; 
			      cell.width = this.tdwidth[i-1];
			}
			
			cell.className="tth";
            row.appendChild(cell);  
        }  
	   
        node.appendChild(row);  

}  ,
	 //添加表格事件方法  
    _AddMouseEvent:function(){  
	    addEventHandler(this.Window,"keydown",BindAsEventListener(this, this._KeyMove));  
		 addEventHandler(this.Document,"click",BindAsEventListener(this, this._MouseOutClick));  
		 addEventHandler(this.Table,"click",BindAsEventListener(this, this._MouseClick));

    },  
	
	 //获取当前事件对象  
    _GetCurrentElem:function(e){  
        var elem = e.srcElement || e.target;  
        if(elem.tagName.toUpperCase()=="TD"){  
            return elem.parentNode;  
        }  
        if(elem.tagName.toUpperCase()=="TR"){  
            return elem;  
        }  
        return null;  
    },  
    prevpage:function(){
    	 
   	this._pageIndex--;
	this._CurrentIndex==-1;
	this.onKeymove();
	this.showall();
    },
    nextpage:function(){
    	 this._pageIndex++;
     	   this._CurrentIndex==-1;
     	   this.onKeymove();
     	   this.showall();
    },
	_KeyMove:function(e){
		
	//	document.selection? document.selection.empty() : //window.getSelection();  
      
	    var index = this._CurrentIndex;
		
		  if(this._CurrentIndex<0)
		return;
		
		if(document.activeElement.id!="")
		 return;
		switch (e.keyCode) {
     case 38 ://上
	    
      this.Table.rows[index].style.backgroundColor = "transparent";
      index=index>1?index-1:1;
	  this.Table.rows[index].style.backgroundColor = this.ActiveColor;
     this._CurrentIndex = index;
	 this.OnClick();  
      break;
     case 40 ://下
	
      var rowl=this.Table.rows.length-1;
	 
	    this.Table.rows[index].style.backgroundColor = "transparent";
     
      index=index<rowl?index+1:rowl;
       this.Table.rows[index].style.backgroundColor = this.ActiveColor;
	   this._CurrentIndex = index;
	     this.OnClick();  
      break;
	 case 39: //right
	    
	   this._pageIndex++;
	   this._CurrentIndex==-1;
	   this.onKeymove();
	   this.showall();
	  break; 
	 case 37: //left
	    this._pageIndex--;
		this._CurrentIndex==-1;
		this.onKeymove();
		this.showall();
		break; 
     default :
      return;
    }

		
	},
  
  
	 _MouseOutClick:function(e){  
         var elem=this._GetCurrentElem(e);  
		
        if(elem!=null){return;}  
		elem = e.srcElement || e.target;  
		
		if(elem.tagName.toUpperCase()=="INPUT"||elem.tagName.toUpperCase()=="BUTTON")
		   return;
		  var index = this._CurrentIndex;

		  if(this._CurrentIndex<0)
		return;
		if(this.Table.rows[index]!=undefined)
		   this.Table.rows[index].style.backgroundColor = "transparent";
		 this._CurrentIndex = -1;
		// this.OnClick(); 
		
		
    },  
    //选中事件  
    _MouseClick:function(e){  
        var elem=this._GetCurrentElem(e);  
		
        if(elem==null){return;}  
      
	    if(this._CurrentRow!=null)
		{
		  	
			  this._CurrentRow.style.backgroundColor="transparent";  
		}
		this._CurrentIndex = this._GetSelectedIndex(elem);
      
        elem.style.backgroundColor=this.ActiveColor;  
        this._CurrentRow = elem;
        this.OnClick();  
      
    },  
  
	//获取当前行所在索引
	 _GetSelectedIndex:function(elem){  
        for(var i=0;i<this.Table.rows.length;i++){  
            if(elem==this.Table.rows[i]){  
                return i;  
            }  
        }  
        return -1;  
    },  
   
    //获取行所在父节点  
    _GetParentNode:function(){  
	
        var nodes=this.Table.childNodes;  
	
        if(nodes.length>0){  
            for(var i=0;i<nodes.length;i++){  
                if(nodes[i].tagName.toUpperCase()=="TBODY"){  
				
                    return nodes[i];  
                }  
            }  
        }  
        return this.Table;  
    },  
	refreshfromsql:function(){
		var result = this._Cursor;
		delete this._dataobject;
		this._dataobject = new Array();
		
		for(var i=0; i<result.rows.length;i++)
		{
			this._dataobject[i] = result.rows.item(i);
		}
		//this._dataobject.length = result.rows.length;
		delete this._Cursor;
		this.setmaxpage();
		this.showall();
		
	},
	setmaxpage:function(){
	     if(this._dataobject.length<=0)
		 {
		    this._maxpage = 0;
		    return;
		 }
			 this._maxpage = Math.floor(this._dataobject.length/this.pageSize); 
			 
			 this._maxpage =( this._dataobject.length%this.pageSize)==0?this._maxpage:this._maxpage+1;
			 
	},
	showall:function(){
		
	
		var param = this.colname;
	
		var node=this._GetParentNode();  
	
		var result = this._dataobject;
		/*
	    if(this._pageIndex<1)
		   this._pageIndex = 1;
		if(this._pageIndex>this._maxpage)
		  this._pageIndex = this._maxpage;
		if(this.setpage)
		this.Document.getElementById(this.setpage).innerHTML =  this._pageIndex;*/
		if(this._maxpage == 0)
		{
			return;
		}
	   
	     this.DeleteAllRows();
	
		 for(var i=0;i<result.length;i++)
		 {
			  var arow=[];
			  var row = result[i];
		        for(var j=0;j<param.length;j++)
				{
			    arow.push(eval("row."+param[j]));
				}
				this.AddNewRow(arow);
				
		 }
		
	},
	 
	InsertNewRow0:function(content){
		
		
		var param = this.colname;
		
		var row = {};
		
		 for(var j =0;j<param.length;j++)
	     {
			eval("row."+param[j] +"=\"" +content[j]+"\"");
		 }
		
		if(this._dataobject.length%this.pageSize == 0)
		{
			this._maxpage++;
		}
		this._dataobject.unshift(row);
		
		this._pageIndex = 0;
		this.showall();
	},
	InsertNewRow:function(content){ 
		if(this._dataobject.length%this.pageSize == 0)
		{
			this._maxpage++;
			this._pageIndex = this._maxpage;
			this.DeleteAllRows();
			this.AddNewRow(content);			 
		}
		else
		{
		    this._pageIndex = this._maxpage;
		    this.showall();
		    this.AddNewRow(content);
		}
		
		var param = this.colname;
		
		var row = {};
		
		 for(var j =0;j<param.length;j++)
	     {
			eval("row."+param[j] +"=\"" +content[j]+"\"");
		 }
		
		this._dataobject.push(row);
		
		
	},
    //添加行,参数为添加内容数组
    AddNewRow:function(content){ 
        var node=this._GetParentNode(); 
		var len = node.children.length;
		
        var row=document.createElement("tr");  
		
        for(var i=0;i<content.length+1;i++){  
            var cell=document.createElement("td");  
			cell.className="ttd";
		    if(i==0)
			{
			cell.style.display= "none";  
            cell.innerHTML=content[i];  
            row.appendChild(cell);  
			}
			else if(i==1)
			{
				
				cell.innerHTML=len;
				len++;
				row.appendChild(cell);  
			}
			else
			{
			
				 if(typeof(this.colformat[i-1])!="undefined"&&this.colformat[i-1]!=0)
				   
				    content[i-1] = Number(content[i-1]).toFixed(this.colformat[i-1]);	
				if(this.tdwidth[i-1]==0)
				    cell.style.display= "none"; 
				cell.innerHTML=content[i-1];  
                row.appendChild(cell);  
			}
        }  
        node.appendChild(row);  
		
    },  
    //删除选中行  
    DeleteRow:function(){  
	  var node=this._GetParentNode();  
	   
	  if(this._CurrentIndex==-1)
	  return;
	 
		
	   var row  = node.rows[this._CurrentIndex];
	    row.style.backgroundColor="transparent"; 
		
	   node.deleteRow(this._CurrentIndex); 
	   if(this._dataobject.length<=1)
	   {
		   this._dataobject = [];
	   }
	   else
	   {
		  
		var inc = this.pageSize*(this._pageIndex-1)+this._CurrentIndex-1;  
		
		this._dataobject.splice(inc,1);
		this.setmaxpage();
		
		this.showall();
	   }
		
	   this._CurrentIndex = -1;
	    
    },  
	DeleteAllRows:function(){
		 var node=this._GetParentNode(); 
		
		 for(var i =node.rows.length-1;i>0;i--)
		 {
			  node.deleteRow(i); 
		 }
	},
    DeleteTH:function(){
		 var node=this._GetParentNode(); 
		
	     node.deleteRow(0); 
		 
	},
    //修改行,参数为添加内容数组,当启用多选时无效  
    UpdateRow:function(fields,values){  
	 
         if(this._CurrentIndex==-1||this._dataobject==""||fields==""||values=="")
	  return;
     
	    var inc = this.pageSize*(this._pageIndex-1)+this._CurrentIndex-1;  
		
		var oldrow = this._dataobject[inc];
		var row = {};
		row = clone(oldrow);
		for(var i=0;i<fields.length;i++)
		{
		   eval("row."+fields[i] +"=\"" +values[i]+"\"");
		}
		this._dataobject[inc] = row;
	   
    },  
	//去掉行选择
	ClearRowSelection:function(){
		 var node=this._GetParentNode();  
	   
	  if(this._CurrentIndex==-1)
	  return;
	 
	
	var row  = node.rows[this._CurrentIndex];
	    row.style.backgroundColor="transparent"; 
	
	  
	   this._CurrentIndex = -1;
	}
   
};  