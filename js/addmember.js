// JavaScript Document
var $ = function (id) {  
    return "string" == typeof id ? document.getElementById(id) : id;  
}



var AddTemplate = Class.create();  
AddTemplate.prototype = {  
    //初始化  
    Init:function(options){  
	    this.Document = document;
		this.Window = window;
	    this.SetOptions(options);//设置参数 
		this.sqlProvider = this.Options.sqlProvider;
	    this.sqltablename = this.Options.sqltablename;
		this.sqltableitem = this.Options.sqltableitem;
	
		this.tableitem = this.Options.tableitem;
		this.nullmsg = this.Options.nullmsg;
          
        this.Prepage=this.Options.prevpage;  
        this.Nextpage=this.Options.nextpage;  
       
		this._pageIndex = 0;//当前页
    },  
	
	//设置参数  
    SetOptions:function(options){  
        this.Options={  
            prevpage:"0",        //前一页  
            nextpage:"0",       //后一页 
			sqltablename:"",
			sqltableitem:[],
			tableitem:[],
			nullmsg:[]
		
        };  
        Extend(this.Options,options||{});  
    }, 
	goprevpage:function(){
		 parent._ifContent.src = "./page/goods.html";
	},
	
	//添加数据
	addtodatabase:function()
	{
		
		for(var i=0;i<this.sqltableitem.length;i++)
		{
			var pageitem = this.tableitem[i];
			if(pageitem.value==""&&this.nullmsg[i]!="")
			{
			$(pageitem).placeholder = this.nullmsg[i];
			//$(pageitem).focus();
			return;
			}
			
		}
	//商品条码，商品名称，商品单价存到数据库中  
	
	var loadstring = "select * from "+this.sqltablename;
	loadstring += " WHERE ";
	loadstring += this.sqltableitem[0];
	loadstring +="=?";
	

	var insertvalue = new Array();
	for(var i=0;i<this.sqltableitem.length;i++)
	{
		
		insertvalue.push($(this.tableitem[i]).value); 
		
	}
	sqltableitem = this.sqltableitem;
	
	var sqltablename = this.sqltablename;
	this.sqlProvider.loadTable(loadstring,[this.tableitem[0]],function(result){	
		if(result.rows.length==0)
		{
			
			this.sqlProvider.insertRow(sqltablename,sqltableitem,insertvalue,function(id)
				{
					 window.parent.gotopage("memberlist");
				
				});
		}
		else
		{
			alert("手机号不可重复");
		
		}
	}); 
	},
	//修改数据
	updatetodatabase:function()
	{
		var insertvalue = new Array();
	
	for(var i=0;i<this.sqltableitem.length;i++)
	{
		
		insertvalue.push($(this.tableitem[i]).value); 
		
	}
		sqltableitem = this.sqltableitem;
	
	var sqltablename = this.sqltablename;
	//商品条码，商品名称，商品单价存到数据库中  
	this.sqlProvider.updateRow(sqltablename,sqltableitem,insertvalue,parent._selectedrowid,function(id)
				{
					 window.parent.gotopage("memberlist");
				//return to prev 
				//	this.table.InsertNewRow([id,code_id,goods_name,goods_unit,goods_sellperprice,goods_discount]); 
					
				//	hiddendiv('LayerUpdate'); 
					//sAlert(null,{width:380,height:120,left:322,top:240},"添加成功!",1); 
			//		showAllData();
				});
	
	
	},
	//删除数据库里的数据
	deletetodatabase:function()
	{
		
		var sqltablename = this.sqltablename;
	    this.sqlProvider.deleteRow(sqltablename,parent._selectedrowid,function(result){
			 window.parent.gotopage("memberlist");
			});
	},
	//初始化修改数据
	updateinit:function()
	{
		
		
	//商品条码，商品名称，商品单价存到数据库中  
	
	var loadstring = "select * from "+this.sqltablename;
	loadstring += " WHERE id=?";
	
	
	sqltableitem = this.sqltableitem;
	tableitem = this.tableitem;
	
	var sqltablename = this.sqltablename;
	this.sqlProvider.loadTable(loadstring,[parent._selectedrowid],function(result){	
	
		if(result.rows.length==1)
		{
			var row = result.rows[0];
			for(var i=0;i<sqltableitem.length;i++)
		    {
			  var pageitem = tableitem[i];
			  var param = sqltableitem[i];
			  $(pageitem).value = eval("row."+param);
			  
			// window.parent.gotopage("teatype");
			
		    }
			
		}
		else
		{
			alert("此商品不存在");
		//	showmsg("LayerUpdate","code_id","商品条码不可重复");						
		//	$("code_id").focus();
			//return;
		}
	}); 
	},
};  
function adddata()
{
	addInstance.addtodatabase();
}
function updatetodatabase()
{
	addInstance.updatetodatabase();
}
function deletedata()
{
	addInstance.deletetodatabase();
}
function cancelclick()
{
	 window.parent.gotopage("memberlist");
}

function init()
{
	try{
	this.sqlProvider = window.parent.sqlProvider;
	}
	catch(e){
	}
	var divinner;
	divinner = "<table style='width:1000px;'>  ";
	divinner += "<tr>";
	divinner += "<td height='50px' width='100px' align='right'>";
	divinner += "<p>手机号码</p></td>";
	divinner += "<td width='250px' align='left'>";
	divinner += "<input type='text' id='tel' placeholder='请输入手机号码'></td></tr>";
	divinner += "<tr>";
	divinner += "<td height='50px' width='100px' align='right'>";
	divinner += "<p>姓名</p></td>";
	divinner += "<td width='250px' align='left'>";
	divinner += "<input type='text' id='name' placeholder='请输入姓名，可省略'></td></tr>";
	divinner += "<tr>";
	divinner += "<td height='50px' width='100px' align='right'>";
	divinner += "<p>积分</p></td>";
	divinner += "<td width='250px' align='left'>";
	divinner += "<input type='text' id='credits' disabled value='0'></td></tr>";
	divinner +=  "<tr><td  height='45px' colspan='4' align='center'>"
	divinner += "<input type='button' id='addsure' value='确定'>&nbsp;&nbsp;<input id='cancel' type='button' value='取消'>";
    divinner += "</td></tr></table>";
	
	
	this.addInstance=new AddTemplate({ sqltablename:"MEMBERCARD_INFO",sqltableitem:["tel","name","credits"],tableitem:["tel","name","credits"],nullmsg:["手机号码不可为空"],sqlProvider:this.sqlProvider});
	addInstance = this.addInstance;
	
	if(parent._operatetype=="add") 
	{
		$("LayerAdd").innerHTML = divinner;
		//$("tel").focus();
		addEventHandler($("addsure"),"click",adddata); 
	}
	else if(parent._operatetype=="update")
	{
		$("LayerAdd").innerHTML = divinner;
		addInstance.updateinit();
		addEventHandler($("addsure"),"click",updatetodatabase); 
	}
	else if(parent._operatetype=="delete")
	{
		divinner = "<table style='width:1000px;'>  ";
		divinner += "<tr>";
		divinner += "<td height='50px' width='100px' align='right'>";
		divinner += "<p>确定删除吗？</p></td></tr>";
	
		
		divinner +=  "<tr><td  height='45px' colspan='4' align='center'>"
		divinner += "<input type='button' id='addsure' value='确定'>&nbsp;&nbsp;<input type='button' value='取消' id='cancel'>";
		divinner += "</td></tr></table>";
		
		$("LayerAdd").innerHTML = divinner;
		addEventHandler($("addsure"),"click",deletedata); 
		
	}
	addEventHandler($("cancel"),"click",cancelclick); 
}
var addInstance;
window.onload = init;